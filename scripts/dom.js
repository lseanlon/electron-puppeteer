
const Utility = require('./utility');

function getReports() {
    getKibanaReports();
    getBehatReports();
}

function getBehatReports() {
    const fs = require('fs');

    let rawdata = fs.readFileSync('./config/behat-link.json');
    let reportList = JSON.parse(rawdata);
    for (var i = 0; i < reportList.length; i++) {
        Utility.downloadImage(reportList[i].link, reportList[i].name + '.png', 'behat-image-report', reportList[i])
    }


}
function getKibanaReports() {
    const fs = require('fs');

    let rawdata = fs.readFileSync('./config/kibana-link.json');
    let kibanaLinks = JSON.parse(rawdata);

    rawdata = fs.readFileSync('./config/kibana-access.json');
    let kibanaAccess = JSON.parse(rawdata);

    // login 

    Utility.login(kibanaAccess.loginUrl, kibanaAccess.username, kibanaAccess.password,kibanaLinks);




}
document.getElementById('btnReport').onclick = () => {
    getReports();
}
document.getElementById('btnKibanaReport').onclick = () => {
    getKibanaReports();
}

document.getElementById('btnBehatReport').onclick = () => {
    getBehatReports();
}