

const puppeteer = require('puppeteer');
// url : http://example.org
// filename : filename.png
// url : http://example.org
const downloadImage = (url, filename, domId, itemInfo) => {

  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    const options = { fullPage: true, path: './downloaded/' + filename };
    console.log('options', options);
    await page.setViewport({ width: 1000, height: 500 });
    await page.goto(url);
    // await page.waitForNavigation({ timeout: 7 * 1000, waitUntil: 'domcontentloaded' });
    // await page.screenshot({ fullPage: true, path: './downloaded/' + filename });
    const b64string = await page.screenshot({ encoding: "base64" });



    var section = document.createElement("section")

    var h3 = document.createElement("H3")
    var text = document.createTextNode(itemInfo.name);
    h3.appendChild(text);
    section.append(h3);

    const imageItem = new Image();
    imageItem.src = 'data:image/png;base64,' + b64string;
    section.append(imageItem);

    document.getElementById(domId).append(section);

    await page.waitFor(1000);


    await browser.close();
  })();
}

const doLoginScreenshot = async (page, url, filename, domId, itemInfo) => {
  await page.goto(url);

  await page.waitFor(4000);
  // await page.waitForNavigation({ timeout: 7 * 1000, waitUntil: 'domcontentloaded' });
  // await page.screenshot({ fullPage: true, path: './downloaded/' + filename });
  const b64string = await page.screenshot({ encoding: "base64" });



  var section = document.createElement("section")

  var h3 = document.createElement("H3")
  var text = document.createTextNode(itemInfo.name);
  h3.appendChild(text);
  section.append(h3);

  const imageItem = new Image();
  imageItem.src = 'data:image/png;base64,' + b64string;
  section.append(imageItem);

  document.getElementById(domId).append(section);

  await page.waitFor(2000);


}


const login = (url, username, password, kibanaLinks) => {

  (async () => {
    const browser = await puppeteer.launch(
      {
        headless: false,
        'defaultViewport': { 'width': 1200, 'height': 720 }
      });

    const username_selector = 'body > div.container > div > div.modal-content.background-customizable.modal-content-mobile.visible-md.visible-lg > div.modal-body > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(3) > input';
    const password_selector = 'body > div.container > div > div.modal-content.background-customizable.modal-content-mobile.visible-md.visible-lg > div.modal-body > div:nth-child(2) > div:nth-child(2) > div > div > form > div:nth-child(5) > input';
    const button_selector = 'body > div.container > div > div.modal-content.background-customizable.modal-content-mobile.visible-md.visible-lg > div.modal-body > div:nth-child(2) > div:nth-child(2) > div > div > form > input.btn.btn-primary.submitButton-customizable';
    const page = await browser.newPage();
    page.setDefaultNavigationTimeout(90000);
    await page.setViewport({ width: 1200, height: 720 })
    await page.setUserAgent('UA-TEST');
    await page.goto(url, { waitUntil: 'domcontentloaded' }); // wait until page load

    // await page.waitForSelector(username_selector);
    // await page.waitForSelector(password_selector);
    // await page.waitForSelector(button_selector);
    await page.click(username_selector);
    await page.type(username_selector, username);

    await page.click(password_selector);
    await page.type(password_selector, password);

    // click and wait for navigation
    // return Promise.all([ ]);
    await page.click(button_selector);
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' }).then(async () => {

      await page.waitFor(1000);
      await page.screenshot({ fullPage: true, path: './downloaded/loginsuccess.png' });
      // go to link and screenshopt
      for (var i = 0; i < kibanaLinks.length; i++) {
        await doLoginScreenshot(page, kibanaLinks[i].link, kibanaLinks[i].name + '.png', 'kibana-image-report', kibanaLinks[i])
      }
    });




  })();

}

module.exports = {
  login: login,
  downloadImage: downloadImage
}
// downloadImage('http://example.org','example.png','')